
import java.sql.*;


public class PostgreCon {

    public static Connection GetConnection( String port, String user, String password )
    {
         Connection conn = null;
        try {
            // String host = "jdbc:postgresql://localhost/domi";
             String host = "jdbc:postgresql://[::1]:";
            host = host + port + "/";
            Connection con = DriverManager.getConnection(host, user, password);
                
            conn = DriverManager.getConnection(host, user, password);
            return conn;
        } 
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
 
        return conn;
    }
    
     public static ResultSet RunQueryConnection( Connection con, String query )
    {
        ResultSet rs = null;
        try
        {  
                Statement stmt=con.createStatement();  
                rs=stmt.executeQuery(query);  
        }
        catch(Exception e)
        {
            System.out.println(e);
        }  
        return rs;
    }
     
}
