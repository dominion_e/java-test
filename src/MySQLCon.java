import java.sql.*;   
/**
 *
 * @author Dominion
 */
public class MySQLCon 
{
    
    public static Connection GetConnection( String port, String user, String password )
    {
        try{  
                Class.forName("com.mysql.jdbc.Driver");  
                String host = "jdbc:mysql://localhost:";
                host = host + port;
                Connection con = DriverManager.getConnection(host, user, password);
                
                return con;
            }
        catch(Exception e)
        {
            System.out.println(e);
            return null;
        }  
    }
    
    public static ResultSet RunQueryConnection( Connection con, String query )
    {
        ResultSet rs = null;
        try
        {  
                Statement stmt=con.createStatement();  
                rs=stmt.executeQuery(query);  
                while(rs.next())  
                System.out.println(rs.getInt (1)+"  "+rs.getString(2)+"  "+rs.getString(3)); 
        }
        catch(Exception e)
        {
            System.out.println(e);
            
        }  
        return rs;
    }

  
    
}
